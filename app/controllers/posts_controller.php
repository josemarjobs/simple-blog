<?php 
	$app->get("/posts", function() use($app){
		render([
			'view' 	=> 'posts/index.php',
			'title'	=> 'All Posts'	
		]);
	});

	$app->get("/posts/i:id", function($id) use($app){
		render([
			'view' 	=> 'posts/show.php',
			'title'	=> 'Post #' . $id	
		]);
	});

	$app->get("/posts/new", function() use($app){
		render([
			'view' 	=> 'posts/new.php',
			'title'	=> 'New Post'
		]);
	});

	$app->get("/posts/:id/edit", function($id) use($app){
		render([
			'view' 	=> 'posts/edit.php',
			'title'	=> 'Edit Post #' . $id
		]);
	});

	function render($options){
		GLOBAL $app;
		$app->render('home.php', $options);
	}
 ?>