<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?></title>
	<link rel="stylesheet" href="/app/assets/css/main.css">
</head>
<body>
	<?php if (isset($view)): ?>
		<?php include "{$view}"; ?>
	<?php endif ?>
</body>
</html>