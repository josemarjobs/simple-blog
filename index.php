<?php 	
	require_once 'vendor/autoload.php';
	include 'init.php';

	$app = new Slim\Slim(array('templates.path' => './app/views'));
	
	$app->get('/', function() use($app){
		$app->render("home.php", [
			'title' => 'My Simple Blog'
		]);
	});

	$controller_path = './app/controllers';
	$controllers_dir = dir($controller_path);
	while(false !==($controller = $controllers_dir->read())){
		if ($controller==".." || $controller==".") { continue; }
		require_once "{$controller_path}/{$controller}";
	}
	
	$app->run();
 ?>